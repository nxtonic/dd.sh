# `dd.sh`: a dear diary shell script

_Not to be confused with `dd`, which is a potentially destructive builtin._

---

### tl;dr

This script helps you maintain a gpg-encrypted diary.

You need:

- a `gpg` executable.
- a `gpg` key that is a viable default recipient.
- a text editor defined by `$EDITOR` _(that can transparently edit `.asc` files)_.

---

## What is this?

`dd.sh` is a bash script that stores a gpg encrypted diary entry for the
whatever the current day is. It adds hourly headers automatically and enables
you to log your thoughts freely with the peace of mind that everything is
stored in gpg-encrypted ascii armor documents.


## Why does it exist?

I decided I wanted to start journaling, and also wanted to roll my own
solution. This is fairly simple and relatively effective, or at least it is
good enough for me.


## Dependencies?

You'll want to have `$EDITOR` set to something that can decrypt and edit a
`.asc` file _transparently_. You'll also want to have `gpg` installed AND have
an existing key that is usable as the default recipient.

- ### `vi` / `vim` / `neovim`
  You'll probably need the [vim-gnupg](https://vimawesome.com/plugin/gnupg-vim)
  plugin which handles the automatic decryption so that you can edit the
  plaintext instead of ciphertext.

- ### `emacs`
  Not being an `emacs` user, I have no idea about it, but from a brief amount
  of research, it looks like: [EasyPG](https://www.emacswiki.org/emacs/EasyPG)
  would suffice for a similar experience as above.

- ### `vscode`
  A quick google found this extension:
  [vscode-gpg](https://github.com/jvalecillos/vscode-gpg) although I can't say
  I recommend following the practice of storing your passphrase in plaintext
  JSON configuration (written in that project's readme), but that's entirely up
  to you.


## Usage

Basically usage is as simple as running `dd.sh` in your shell and the script
will open a file (creating it if it does not exist) for you in the relevant
point in time automatically, once you have sucessfully authenticated against
the gpg prompt. A new file is created for each day with the date stamped into
the filename and in the front matter of the document.
