#!/usr/bin/env bash
# dd.sh

# {{{ setup / defaults


set -euf -o pipefail;

when=$(date "+%Y-%m-%d");
hour=$(date "+%H");

prefix="dd.sh";
store="$HOME/.local/share/.${prefix}/";
slug="${when}.md.asc";
today_md="${store}${slug}";

front_matter=$(envsubst <<END
+++\ndate = "$when"\n+++\n\n
END
);


# }}}
# {{{ pre-handlers / misc


# Set the window title for the application (cleanup function will undo this behaviour).
printf '\033k%s\033\\' "${prefix}"

# enable alias expansion (needed for below case statement).
shopt -s expand_aliases

# augment editor with additional options depending on executable.
case $EDITOR in
  "vi"|"vim"|"nvim"|"gvim") # Run vimscript to jump cursor to a good location.
    alias launcher="${EDITOR} -c 'exe \"normal GA\"' -c 'startinsert'";
    ;;
  *) # If not defined elsewhere, simply use the binary on its own.
    alias launcher=$EDITOR;
    ;;
esac


#}}}
# {{{ functions


# Unset the window title upon cleanup.
cleanup () {
  printf '\033k\033\\'
}

# Add something to the current day's file
append () {
  IFSBAK=$IFS; IFS=" "; pre=$(gpg -d ${today_md} 2> /dev/null);
  echo -en "${pre}$1" | gpg -ea --default-recipient-self 2> /dev/null > ${today_md};
  IFS=$IFSBAK; true;
}

# If the file does not contain a line that describes the current hour, add one that does (and also surrounding newlines).
add_time () {
  is_time=$(gpg -d ${today_md} 2> /dev/null | grep "# ${hour}:00" &> /dev/null; echo $?);
  [ $is_time -ne 0 ] && { append "\n\n# ${hour}:00\n\n\n"; }; true;
}

# Check that the target file can be decrypted, if it can't, then anything after this is probably not going to work, so bail out.
pre_test () {
  pre_test=$(gpg -d ${today_md} &> /dev/null; echo $?);
  [ $pre_test -ne 0 ] && exit 1; true;
}


# }}}
# {{{ post-handlers


# Add a handler for err, exit and kill signals.
trap cleanup err exit kill;


# }}}
# {{{ State responses


[ ! -d ${store} ] && { \
  mkdir -p ${store}; \
  chmod -R 700 ${store}; };

[ ! -f "${today_md}" ] && { \
  echo -en $front_matter | gpg -ea --default-recipient-self 2> /dev/null > ${today_md}; \
  pre_test; add_time; \
  $EDITOR -c 'exe "normal G$"' ${today_md};
  exit; };

[ -f "${today_md}" ] && { \
  pre_test; add_time; \
  launcher ${today_md};
  exit; };


# }}}

# vim: fdm=marker
